### Checkout - Handling Fee

Implement Handling Fee to be displayed in checkout and applied to the order. Requirements:

* Handling fee will be $10 (this can be hardcoded, no need to set configuration value)
* Handling fee will be displayed in checkout in totals, and applied after taxes (no tax apply to the handling fee itself)
* Handling fee has to be applied to order total
* Handling fee has to be displayed in order detail in admin