<?php
namespace Nover\HandlingFee\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

class CustomFeeConfigProvider implements ConfigProviderInterface
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $customFeeConfig = [];
        $customFeeConfig['fee_label'] = __('Handling Fee');
        $quote = $this->checkoutSession->getQuote();
        $customFeeConfig['custom_fee_amount'] = 10;
        $customFeeConfig['show_hide_customfee_block'] = ($quote->getFee()) ? true : false;
        $customFeeConfig['show_hide_customfee_shipblock'] = true;
        return $customFeeConfig;
    }
}
