<?php


namespace Nover\ProductImport\Helper;

use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\CatalogImportExport\Model\Import\Product\CategoryProcessor;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\ImportExport\Model\Import;
use Magento\ImportExport\Model\Import\Source\CsvFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\CategoryRepository;
use Nover\ProductImport\Model\ProductImport;

class Csv extends AbstractHelper
{
    const CATEGORY_MULTIPLE_VALUE_SEPARATOR = '->';
    const ALLOW_ON_WEB_NO = 'No';
    const ALLOW_ON_WEB_COLUMN_NAME = 'Allowed on Web';
    const SKU_COLUMN_NAME = 'SKU';
    const PRICE_COLUMN_NAME = 'Price';
    const PRODUCT_NAME_COLUMN_NAME = 'Product Name';
    const INVENTORY_A_COLUMN_NAME = 'Inventory A';
    const INVENTORY_B_COLUMN_NAME = 'Inventory B';
    const CATEGORY_COLUMN_NAME = 'Category';
    const WEIGHT_COLUMN_NAME = 'Weight';

    /**
     * Name of folder in var/
     */
    const IMPORT_DIR = 'import/';

    /**
     * @var string Name of CSV file.
     */
    private $importedFile = 'products.csv';

    /**
     * @var CsvFactory
     */
    private $sourceCsvFactory;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var CategoryProcessor
     */
    protected $categoryProcessor;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * Name of default category.
     *
     * @var null
     */
    protected $defaultCategoryName = null;

    /**
     * @var ProductImport
     */
    protected $productImport;

    /**
     * Csv constructor.
     *
     * @param Context               $context
     * @param CsvFactory            $sourceCsvFactory
     * @param Filesystem            $filesystem
     * @param CategoryProcessor     $categoryProcessor
     * @param StoreManagerInterface $storeManager
     * @param CategoryRepository    $categoryRepository
     * @param ProductImport         $productImport
     */
    public function __construct(
        Context $context,
        CsvFactory $sourceCsvFactory,
        Filesystem $filesystem,
        CategoryProcessor $categoryProcessor,
        StoreManagerInterface $storeManager,
        CategoryRepository $categoryRepository,
        ProductImport $productImport
    ) {
        parent::__construct($context);
        $this->sourceCsvFactory = $sourceCsvFactory;
        $this->filesystem = $filesystem;
        $this->categoryProcessor = $categoryProcessor;
        $this->storeManager = $storeManager;
        $this->categoryRepository = $categoryRepository;
        $this->productImport = $productImport;
    }

    /**
     * Update the product from the CSV data.
     *
     * @return $this
     * @throws LocalizedException
     */
    public function import()
    {
        try {
            $this->processCsvFile();
        } catch (\Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }

        return $this;
    }

    /**
     * Create data source from CSV file.
     *
     * @param  string $sourceFile
     * @return \Magento\ImportExport\Model\Import\Source\Csv
     */
    public function createSource($sourceFile)
    {
        $obj = $this->sourceCsvFactory->create(
            [
                'file' => $sourceFile,
                'directory' => $this->filesystem->getDirectoryRead(DirectoryList::VAR_DIR)
            ]
        );

        return $obj;
    }

    /**
     * Get data from the CSV file.
     *
     * @return array
     * @throws LocalizedException
     */
    public function processCsvFile()
    {
        $read = $this->filesystem->getDirectoryRead(DirectoryList::VAR_DIR);
        $fileCsvPath = self::IMPORT_DIR . $this->importedFile;

        try {
            if (!$read->isExist($fileCsvPath)) {
                throw new LocalizedException(__('File does not exist!'));
            }

            return $this->validateSource(
                $this->createSource($fileCsvPath)
            );
        } catch (\Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }
    }

    /**
     * Validate data from the CSV.
     *
     * @param  $dataSource
     * @throws LocalizedException
     */
    protected function validateSource($dataSource)
    {
        try {
            if (empty($dataSource)) {
                throw new LocalizedException(__('Cannot initial data source from the CSV file!'));
            }
            $validData = [
                'disabled' => [],
                'enabled' => []
            ];

            $flag = 0;
            foreach ($dataSource as $rowNum => $rowData) {
                $flag++;
                $data = array_map('trim', $rowData);

                if (empty($data[self::ALLOW_ON_WEB_COLUMN_NAME])) {
                    continue;
                }

                if (strtolower(trim($data[self::ALLOW_ON_WEB_COLUMN_NAME]))== strtolower(trim(self::ALLOW_ON_WEB_NO))
                    && !in_array($data[self::SKU_COLUMN_NAME], $validData['disabled'])
                ) {
                    $validData['disabled'][] = $data[self::SKU_COLUMN_NAME];
                    continue;
                }

                $temporaryData = [];

                $temporaryData['status'] = Status::STATUS_ENABLED;
                $temporaryData['sku'] = empty($data[self::SKU_COLUMN_NAME]) ? '' : $data[self::SKU_COLUMN_NAME];
                $temporaryData['price'] = empty($data[self::PRICE_COLUMN_NAME]) ? 0 : $data[self::PRICE_COLUMN_NAME];

                $temporaryData['name'] = empty($data[self::PRODUCT_NAME_COLUMN_NAME]) ?
                    '' : $data[self::PRODUCT_NAME_COLUMN_NAME];
                $temporaryData['weight'] = empty($data[self::WEIGHT_COLUMN_NAME]) ?
                    '' : $data[self::WEIGHT_COLUMN_NAME];

                $inventoryA = !empty($data[self::INVENTORY_A_COLUMN_NAME]) ?
                    (int)$data[self::INVENTORY_A_COLUMN_NAME] : 0;
                $inventoryB = !empty($data[self::INVENTORY_B_COLUMN_NAME]) ?
                    (int)$data[self::INVENTORY_B_COLUMN_NAME] : 0;

                $temporaryData['categories'] = $this->processCategories(
                    isset($data[self::CATEGORY_COLUMN_NAME]) ? $data[self::CATEGORY_COLUMN_NAME] : ''
                );

                $temporaryData['stock'] = $inventoryA + $inventoryB;

                $validData['enabled'][$temporaryData['sku']] = $temporaryData;

                if ($flag % ProductImport::BATCH_SIZE_PRODUCT_TO_SAVE == 0) {
                    $this->handleProductData($validData);
                    $validData = [
                        'disabled' => [],
                        'enabled' => []
                    ];
                }
            }

            $this->handleProductData($validData);

        } catch (\Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }
    }

    /**
     * Handle the product data.
     *
     * @param $data
     */
    public function handleProductData($data)
    {
        try {
            $this->productImport->disableProducts(
                isset($data['disabled']) ? $data['disabled'] : []
            );

            $this->productImport->processProducts(
                isset($data['enabled']) ? $data['enabled'] : []
            );
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
        }
    }

    /**
     * Process the categories from string. Created the new categories if they does not exist.
     *
     * @param  $categoriesString
     * @return array
     */
    private function processCategories($categoriesString)
    {
        if (empty($categoriesString)) {
            return [];
        }

        $rootCategoryName = $this->getDefaultCategoryName();

        $categoriesString = str_replace(
            self::CATEGORY_MULTIPLE_VALUE_SEPARATOR,
            CategoryProcessor::DELIMITER_CATEGORY,
            $rootCategoryName . CategoryProcessor::DELIMITER_CATEGORY . $categoriesString
        );

        return $this->categoryProcessor->upsertCategories(
            $categoriesString,
            Import::DEFAULT_GLOBAL_MULTI_VALUE_SEPARATOR
        );
    }

    /**
     * Get name of default category.
     *
     * @return string|null
     */
    private function getDefaultCategoryName()
    {
        if ($this->defaultCategoryName == null) {
            try {
                $rootCategoryId = $this->storeManager->getStore()->getRootCategoryId();

                $defaultCategory = $this->categoryRepository->get(
                    $rootCategoryId,
                    $this->storeManager->getStore()->getId()
                );

                if (empty($defaultCategory)) {
                    return null;
                }

                $this->defaultCategoryName = $defaultCategory->getName();
            } catch (\Exception $ex) {
                return null;
            }
        }

        return $this->defaultCategoryName;
    }
}
