<?php

namespace Nover\ProductImport\Model;

use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Ui\DataProvider\Product\ProductCollectionFactory;
use Magento\Framework\App\Area;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DataObject;
use Magento\Catalog\Model\ResourceModel\Product;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\ResourceModel\Product\Action;
use Magento\CatalogImportExport\Model\Import\Product\CategoryProcessor;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\Store;
use Psr\Log\LoggerInterface;
use Magento\Framework\DB\TransactionFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\State;
use Magento\Catalog\Model\Product\Type;

class ProductImport extends DataObject
{
    const BATCH_SIZE_PRODUCT_TO_SAVE = 1000;
    const CATEGORY_PRODUCT_TABLE_NAME = 'catalog_category_product';
    const CATALOG_PRODUCT_TABLE_NAME = 'catalog_product_entity';
    const ATTRIBUTE_SET_ID_SIMPLE_PRODUCT = 4;

    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * @var Action
     */
    protected $productAction;

    /**
     * @var Product
     */
    protected $productResource;

    /**
     * @var CategoryProcessor
     */
    protected $categoryProcessor;

    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var AdapterInterface
     */
    private $connection;

    /**
     * The name of category product table.
     */
    protected $categoryProductTblName;

    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TransactionFactory
     */
    protected $transactionFactory;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var State
     */
    protected $state;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    protected $stockAndCategoryDataToSave = [];

    protected $productDataToCreate = [];

    /**
     * ProductImport constructor.
     *
     * @param Product                $productResource
     * @param StockRegistryInterface $stockRegistry
     * @param Action                 $productAction
     * @param CategoryProcessor      $categoryProcessor
     * @param ResourceConnection     $resource
     * @param LoggerInterface        $logger
     * @param TransactionFactory     $transactionFactory
     * @param ProductFactory         $productFactory
     * @param State                  $state
     * @param StoreManagerInterface  $storeManager
     * @param array                  $data
     */
    public function __construct(
        Product $productResource,
        StockRegistryInterface $stockRegistry,
        Action $productAction,
        CategoryProcessor $categoryProcessor,
        ResourceConnection $resource,
        LoggerInterface $logger,
        TransactionFactory $transactionFactory,
        ProductFactory $productFactory,
        State $state,
        StoreManagerInterface $storeManager,
        array $data = []
    ) {
        parent::__construct($data);

        $this->stockRegistry = $stockRegistry;
        $this->productAction = $productAction;
        $this->productResource = $productResource;
        $this->categoryProcessor = $categoryProcessor;
        $this->logger = $logger;
        $this->transactionFactory = $transactionFactory;
        $this->productFactory = $productFactory;
        $this->state = $state;
        $this->storeManager = $storeManager;
        $this->resource = $resource;
        $this->connection = $this->resource->getConnection();
    }

    /**
     * Get validated table name
     *
     * @param  string $table
     * @return string
     */
    protected function getTable($table)
    {
        return $this->resource->getTableName($table);
    }

    /**
     * Get entity and skus from the database.
     *
     * @param  $listSKUs
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    protected function getListProducts($listSKUs)
    {
        $listProducts = [];
        $productTable = $this->getTable(self::CATALOG_PRODUCT_TABLE_NAME);
        $select = $this->connection->select();

        $skus = sprintf('"%s"', implode('","', $listSKUs));

        $select->from(
            ['e' => $productTable],
            [
                'entity_id',
                'sku'
            ]
        )->where(
            "sku IN ($skus)"
        )->distinct();

        $query = $this->resource->getConnection()->query($select);
        while ($row = $query->fetch()) {
            $listProducts[$row['sku']] = $row['entity_id'];
        }

        return $listProducts;
    }

    /**
     * Get table name of category product.
     *
     * @return string
     */
    private function getCategoryProductTableName()
    {
        if ($this->categoryProductTblName == null) {
            $this->categoryProductTblName = $this->resource->getTableName(
                self::CATEGORY_PRODUCT_TABLE_NAME
            );
        }

        return $this->categoryProductTblName;
    }

    /**
     * Disable the products.
     *
     * @param  $productSKUs
     * @return $this
     * @throws LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    public function disableProducts($productSKUs)
    {
        if (empty($productSKUs)) {
            return $this;
        }

        $listProducts = $this->getListProducts($productSKUs);

        if (empty($listProducts)) {
            return $this;
        }

        $this->updateAttributes(
            array_values($listProducts),
            ['status' => Status::STATUS_DISABLED]
        );

        return $this;
    }

    /**
     * Update products.
     *
     * @param  $updateProductData
     * @return $this
     * @throws \Exception
     */
    public function processProducts($updateProductData)
    {
        if (empty($updateProductData)) {
            return $this;
        }

        $batches = array_chunk(
            $updateProductData,
            self::BATCH_SIZE_PRODUCT_TO_SAVE,
            true
        );

        if (empty($batches)) {
            return $this;
        }

        foreach ($batches as $batch) {
            $listProducts = $this->getListProducts(
                array_keys($batch)
            );

            $newProductData = array_diff_key(
                $batch,
                $listProducts
            );

            $this->createNewProducts($newProductData);
            $this->updateBatchProducts($batch, $listProducts);
        }

        return $this;
    }

    /**
     * Update the batch of 1000 products.
     *
     * @param  $batch
     * @param  $listProducts
     * @return $this
     */
    protected function updateBatchProducts($batch, $listProducts)
    {
        $this->connection->beginTransaction();
        try {
            foreach ($batch as $sku => $productData) {
                if (!array_key_exists($sku, $listProducts)) {
                    $this->logger->notice(
                        __('SKU %1 not found in system', $sku)
                    );
                    continue;
                }

                $this->updateAttributes(
                    $listProducts[$sku],
                    [
                        'name' => $productData['name'],
                        'price' => $productData['price'],
                        'status' => $productData['status'],
                        'weight' => $productData['weight'],
                    ],
                    $productData['sku']
                );

                $this->stockAndCategoryDataToSave[] = [
                    'productId' => $listProducts[$sku],
                    'sku' => $productData['sku'],
                    'stock' => $productData['stock'],
                    'categories' => $productData['categories'],
                ];
            }

            $this->saveStockAndCategory();
            $this->connection->commit();
        } catch (\Exception $exception) {
            $this->connection->rollBack();
            $this->logger->error($exception->getMessage());
        }

        return $this;
    }

    /**
     * Update Product Attribute.
     *
     * @param  $productIds
     * @param  $data
     * @param  $sku
     * @throws LocalizedException
     */
    private function updateAttributes($productIds, $data, $sku = null)
    {
        $storeId = Store::DEFAULT_STORE_ID;
        try {
            $this->productAction->updateAttributes(
                is_array($productIds) ? $productIds : [$productIds],
                $data,
                $storeId
            );

            $this->logger->info(
                __('Updated SKU %1: %2', $sku, json_encode($data))
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMesage());
            throw new LocalizedException(__($e->getMessage()));
        }
    }

    /**
     * Process Stock and Categories data.
     *
     * @return $this
     * @throws LocalizedException
     */
    private function saveStockAndCategory()
    {
        if (empty($this->stockAndCategoryDataToSave)) {
            return $this;
        }

        foreach ($this->stockAndCategoryDataToSave as $data) {
            $this->updateStock($data['sku'], $data['stock']);
            $this->saveProductCategories(
                $data['productId'],
                $data['categories'],
                $data['sku']
            );
        }

        $this->stockAndCategoryDataToSave = [];

        return $this;
    }

    /**
     * Update the product stock.
     *
     * @param  $sku
     * @param  $qty
     * @throws LocalizedException
     */
    private function updateStock($sku, $qty)
    {
        try {
            $stockItem = $this->stockRegistry->getStockItemBySku($sku);
            $stockItem->setQty($qty);
            $stockItem->setIsInStock($qty > 0);
            $this->stockRegistry->updateStockItemBySku($sku, $stockItem);
            $this->logger->info(
                __('Updated SKU %1: qty: %1', $sku, $qty)
            );
        } catch (\Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }
    }

    /**
     * Save categories to the product.
     *
     * @param  $productId
     * @param  $categories
     * @param  $sku
     * @return $this
     */
    private function saveProductCategories($productId, $categories, $sku)
    {
        if (empty($productId) || empty($categories)) {
            return $this;
        }

        $tableName = $this->getCategoryProductTableName();

        foreach ($categories as $categoryId) {
            $categoriesIn[] = [
                'product_id' => $productId,
                'category_id' => $categoryId,
                'position' => 0
            ];
        }

        if (empty($categoriesIn)) {
            return $this;
        }

        $this->connection->insertOnDuplicate(
            $tableName,
            $categoriesIn,
            ['product_id', 'category_id']
        );

        $this->logger->info(
            __('Updated SKU %1 category ids: %2', $sku, json_encode($categoriesIn))
        );

        return $this;
    }

    /**
     * Create new products.
     *
     * @param  $dataToCreate
     * @return $this
     * @throws \Exception
     */
    private function createNewProducts($dataToCreate)
    {
        if (empty($dataToCreate)) {
            return $this;
        }

        $this->state->setAreaCode(Area::AREA_ADMINHTML);

        $saveTransaction = $this->transactionFactory->create();

        try {
            foreach ($dataToCreate as $sku => $data) {
                $newProduct = $this->productFactory->create();
                $newProduct->setName($data['name']);
                $newProduct->setSku($data['sku']);
                $newProduct->setTypeId(Type::DEFAULT_TYPE);
                $newProduct->setAttributeSetId(self::ATTRIBUTE_SET_ID_SIMPLE_PRODUCT);
                $newProduct->setWebsiteIds([$this->storeManager->getStore()->getWebsiteId()]);
                $newProduct->setVisibility(Visibility::VISIBILITY_BOTH);
                $newProduct->setStatus($data['status']);
                $newProduct->setPrice($data['price']);
                $newProduct->setWeight($data['weight']);

                $saveTransaction->addObject($newProduct);
            }

            $saveTransaction->save();
            $this->updateStockAndCategories($dataToCreate);

            $this->logger->info(
                __('Created new products: %1', json_encode($this->productDataToCreate))
            );
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMesage());
        }

        $this->productDataToCreate = [];

        return $this;
    }

    /**
     * Update the stock and category for new products.
     *
     * @param  $data
     * @return $this
     */
    private function updateStockAndCategories($data)
    {
        if (empty($data)) {
            return $this;
        }

        try {
            $listProducts = $this->getListProducts(
                array_keys($data)
            );

            if (empty($listProducts)) {
                return $this;
            }
            
            foreach ($listProducts as $sku => $productId) {
                $this->stockAndCategoryDataToSave[] = [
                    'productId' => $productId,
                    'sku' => $sku,
                    'stock' => $data[$sku]['stock'],
                    'categories' => $data[$sku]['categories'],
                ];
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMesage());
        }

        return $this;
    }
}
