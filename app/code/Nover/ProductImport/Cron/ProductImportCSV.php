<?php

namespace Nover\ProductImport\Cron;

use Magento\Framework\Exception\LocalizedException;
use Nover\ProductImport\Helper\Csv;

class ProductImportCSV
{
    /**
     * @var Csv
     */
    private $csv;

    /**
     * ProductImportCSV constructor.
     *
     * @param Csv $csv
     */
    public function __construct(
        Csv $csv
    ) {
        $this->csv = $csv;
    }

    /**
     * Process the CSV file.
     *
     * @throws LocalizedException
     */
    public function execute()
    {
        try {
            $this->csv->import();
        } catch (\Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }
    }
}
