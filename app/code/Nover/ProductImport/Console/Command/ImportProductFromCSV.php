<?php


namespace Nover\ProductImport\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Console\Cli;
use Nover\ProductImport\Helper\Csv;

class ImportProductFromCSV extends Command
{
    /**
     * @var Csv
     */
    private $csv;

    /**
     * ImportProductFromCSV constructor.
     *
     * @param Csv  $csv
     * @param null $name
     */
    public function __construct(
        Csv $csv,
        $name = null
    ) {
        parent::__construct($name);
        $this->csv = $csv;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('nover:import:product')
            ->setDescription('Import the products from the CSV file.');

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->csv->import();
            $output->writeln('<info>Imported Successfully!</info>');

            return Cli::RETURN_SUCCESS;
        } catch (\Exception $e) {
            $output->writeln('<error>' . $e->getMessage() . '</error>');

            if ($output->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
                $output->writeln($e->getTraceAsString());
            }

            return Cli::RETURN_FAILURE;
        }
    }
}
