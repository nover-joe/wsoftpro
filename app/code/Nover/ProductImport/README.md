### Product Import

Create an import command to be run through Magento CLI and cron task to regularly (daily) import products from CSV (attached). Requirements:

* We have just simple and configurable products

* Main identifier is SKU

* One SKU can be presented multiple times in the file; in that case later version apply

* Import only products allowed on web
  * If product is `Allowed on Web = Yes`, import
  * If product is `Allowed on Web = No` and
    * exists in Magento, then disable the product
    * does not exist in Magento, then skip
    
* Support existing products in the database


### Command

    php bin/magento nover:import:product

### CSV File
* It should be in `var/import` folder.

### Importing Performance

* Every 1000 rows we will update to the database once until the last rows from the CSV file.

* Almost we use the raw query to update the database quickly.

### Multi Source Inventory (MSI)

* We have not implemented to support MSI yet as our discussion.

### Recommendation

* In the case of SKUs are equal or greater than 100k, we suggest to use queue for importing products (both creation or update) one by one, 
we will also customize for stock and pricing indexer to avoid the issue of catalog indexing.